#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 17:21:17 2018

@author: manuela
"""

from neo4j.v1 import GraphDatabase, basic_auth
import pandas as pd
import MySQLdb
import json

#------------------------------------------------------------------------------ CREAZIONE DATI NODI E ARCHI
db = MySQLdb.connect(host="23.251.137.13",    
                     user="root",        
                     passwd="9Sg&zB3hn5ZeYf",  
                     db="InterestGraph_results",
                     charset='utf8')        
cur = db.cursor()
cur.execute("SELECT cat_1, cat_2 FROM guid_profiles")
fetch = cur.fetchall()
db.close()
big_frame = pd.DataFrame(list(fetch), columns=['cat_1', 'cat_2'])
big_frame = big_frame[(big_frame['cat_1']!='') & (big_frame['cat_2']!='')]

#definisco volume + searchWords del nodo
ambito = []
nodo = []
volume = []
words=[]
lista_cat = list(set(list(big_frame['cat_1'])+list(big_frame['cat_2'])))
for c in lista_cat:
    scomp = c.split(' > ')
    ambito.append(scomp[0].replace(' ', '_'))
    nodo.append(' > '.join(s.lstrip().capitalize().replace("'", "") for s in scomp[1:]))
    volume.append(len(big_frame[(big_frame['cat_1']==c) | (big_frame['cat_2']==c)]))

    db = MySQLdb.connect(host="23.251.137.13",    
                         user="root",        
                         passwd="9Sg&zB3hn5ZeYf",  
                         db="InterestGraph_results",
                         charset='utf8')        
    cur = db.cursor()
    cur.execute("SELECT JSON_EXTRACT(profilo, '$.\"{0}\".search_keywords') as s FROM guid_profiles WHERE cat_1=\"{0}\" OR cat_2=\"{0}\"".format(c.replace("'","\\'")))
    fetch = cur.fetchall()
    db.close()
    words.append(', '.join(list(set([p  for k in range(len(fetch)) for p in json.loads(fetch[k][0]) if p!='']))))     
nodi_grafo = pd.DataFrame({'ambito':ambito, 'node':nodo, 'volume':volume, 'searchWords':words})

# definisco pesi degli archi
ambito1 = []
ambito2 = []
nodo1 = []
nodo2 = []
peso  = []
for c1 in list(set(big_frame['cat_1'])):
    small_frame = big_frame[big_frame['cat_1']==c1]
    for c2 in list(set(small_frame['cat_2'])):
        scomp1 = c1.split(' > ')
        ambito1.append(scomp1[0].replace("'", "").replace(' ', '_'))
        nodo1.append(' > '.join(s.lstrip().capitalize().replace("'", "") for s in scomp1[1:]))
        scomp2 = c2.split(' > ')
        ambito2.append(scomp2[0].replace("'", "").replace(' ', '_'))
        nodo2.append(' > '.join(s.lstrip().capitalize().replace("'", "") for s in scomp2[1:]))
        peso.append(round(len(small_frame[small_frame['cat_2']==c2])*100/len(small_frame), 2))        
archi_grafo = pd.DataFrame({'ambito1':ambito1, 'node1':nodo1, 'ambito2':ambito2, 'node2':nodo2, 'weight':peso})

            
#------------------------------------------------------------------------------ SALVATAGGIO INFO GRAFO
#nodi
try:
    db = MySQLdb.connect(host="23.251.137.13",    
                         user="root",        
                         passwd="9Sg&zB3hn5ZeYf",  
                         db="InterestGraph_results",
                         charset='utf8')        
    cur = db.cursor()
    cur.execute("TRUNCATE TABLE grafo_nodi")
    db.commit()
    db.close()
    db = MySQLdb.connect(host="23.251.137.13",    
                         user="root",        
                         passwd="9Sg&zB3hn5ZeYf",  
                         db="InterestGraph_results",
                         charset='utf8')  
    cur = db.cursor()
    cur.executemany("INSERT INTO grafo_nodi (ambito, node, searchWords, volume) VALUES (%s,%s,%s,%s)", [tuple(x) for x in nodi_grafo.values])
    db.commit()
    db.close()
except:
    db.rollback()
    
#archi
try:
    db = MySQLdb.connect(host="23.251.137.13",    
                         user="root",        
                         passwd="9Sg&zB3hn5ZeYf",  
                         db="InterestGraph_results",
                         charset='utf8')   
    cur = db.cursor()
    cur.execute("TRUNCATE TABLE grafo_archi")
    db.commit()
    db.close()
    db = MySQLdb.connect(host="23.251.137.13",    
                         user="root",        
                         passwd="9Sg&zB3hn5ZeYf",  
                         db="InterestGraph_results",
                         charset='utf8') 
    cur = db.cursor()
    cur.executemany("INSERT INTO grafo_archi (ambito1, ambito2, node1, node2, weight) VALUES (%s,%s,%s,%s,%s)", [tuple(x) for x in archi_grafo.values] )
    db.commit()
    db.close()
except:
    db.rollback()
#------------------------------------------------------------------------------ CREAZIONE GRAFO -INVIO INFORMAZIONI A NEO4J
driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "superneo"))
session = driver.session()

# pulisco ciò che c'è
cypher = ("MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r")
session.run(cypher)
    
# creo nodi
for i in range(len(nodi_grafo)):
    tipo_nodo = nodi_grafo['ambito'][i]
    cypher = ("MERGE (a:"+tipo_nodo+" { node:'"+ nodi_grafo['node'][i] + "', "+"volume:'"+ str(nodi_grafo['volume'][i]) + "', " + "searchWords:'"+ nodi_grafo['searchWords'][i] + "'})")
    session.run(cypher)

# creo archi
for j in range(len(archi_grafo)):
    tipo_nodo1 = "a:"+archi_grafo['ambito1'][j]
    tipo_nodo2 = "b:"+archi_grafo['ambito2'][j]
    cypher = ("MATCH ("+tipo_nodo1+"),("+tipo_nodo2+")"
              "WHERE "+tipo_nodo1.split(':')[0]+".node = '" + archi_grafo['node1'][j] + "' AND "+tipo_nodo2.split(':')[0]+".node = '" + archi_grafo['node2'][j] + "'"
              "CREATE ("+tipo_nodo1.split(':')[0]+")-[:prob {prob_value:'"+str(archi_grafo['weight'][j])+"%"+"'}]->("+tipo_nodo2.split(':')[0]+")")
    session.run(cypher)      
session.close()
 

