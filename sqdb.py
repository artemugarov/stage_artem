#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 14:19:26 2018

@author: artem
"""

import sqlite3

def create_database(database_name, table_name):
    conn = sqlite3.connect(database_name)
    c = conn.cursor()

    ###Creating test table per our data
    c.execute('''CREATE TABLE ''' + table_name +
             ''' (client text, guid text, event text, country text, language text, date text, product_price real, product_brand text)''')
    conn.commit()
    return

def connect_to_database(database_name):
    conn = sqlite3.connect(database_name)
    c = conn.cursor()
    return conn, c

#Inserting rows into a new database from data_new0
def insert_file(filename, database_name, table_name):
    
    db, cursor = connect_to_database(database_name)
    
    print("Start reading/writing lines from", filename)
    with open(filename,"r") as f:
        f.readline()
        linenumber = 0
        for line in f:
            words = line.rstrip('\n').split(',', maxsplit=7)
            
            for i in range(8):
                if '"' in words[i]:
                    words[i] = words[i].replace('"','')
                if "'" in words[i]:
                    words[i] = words[i].replace("'",'')
            
            cursor.execute("INSERT INTO " + table_name + " VALUES ('"+words[0]+"','"+words[1]+"','"+words[2]+"','"+words[3]+"','"+words[4]+"','"+words[5]+"','"+words[6]+"','"+words[7]+"')")
            linenumber += 1
    print("All {0} lines written from {1}".format(linenumber, filename))
    db.commit()
    return


# Check inside database
def check_database(database_name, table_name):
    db, cursor = connect_to_database(database_name)
    
    query = "SELECT count(*) FROM " + table_name
    
    result = cursor.execute(query)
    print("> CHECK: total number of lines in db = ", result.fetchone())
    return


def main():
    
    db_name = "data.db"
    table_name = "data"
    
    # Create database
    create_database(db_name, table_name)
    
    # Loop over files to be read and inserted into the database data.db/data
    for i in range(10):
        name_file = "../../python_progs/project_new/data_new" + str(i) + ".csv"
        
        insert_file(name_file, db_name, table_name)
    
    check_database(db_name, table_name)
    return


if __name__ == "__main__":
    main()



# Loop over files
"""
for i in range(10):
    insert_file()
"""          


