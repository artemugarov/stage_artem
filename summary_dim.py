#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 09:42:41 2018

@author: artem
"""

import sqlite3
import pandas as pd
import gc

"""
cnx = sqlite3.connect('data.db')
query= "SELECT country FROM data WHERE client=='ibs.it'"
df = pd.read_sql_query(query, cnx)

df.replace(to_replace="", value=pd.np.nan, inplace=True)

#df.describe(include='all')
df.isnull().sum()
df.isna().sum()

df.country.unique()
df2=df.country=="nan"
df2.sum()
#Summary

#client
with open('summary2.txt','a') as out :
    out.write("   Short summary for ibs_event:\n")
    out.write(str(df.describe(include='all')))
    out.write('\n\n\n\n')

del df2
del query
gc.collect()



"""

def summary(dim,client,file):
    cnx = sqlite3.connect('data.db')
    query= "SELECT "+str(dim)+" FROM data WHERE client=='"+str(client)+"'"
    df = pd.read_sql_query(query, cnx)
    df.replace(to_replace="", value=pd.np.nan, inplace=True)
    #writing summary
    with open(str(file),'a') as out :
        out.write("   Short summary for "+str(client)+"_"+str(dim)+":\n")
        out.write(str(df.describe(include='all'))+"\n")
        out.write("Number of NaNs: "+str(df.isnull().sum()[0]))
        out.write('\n\n\n\n')
    cnx.close()
    del df
    del query
    gc.collect()
    return

def main():
    dims=["guid","event","country","language","date","product_price","product_brand"]
    clients=["EURONICS","ibs.it"]
    file="summary2.txt"
    for client in clients:
        for dim in dims:
            summary(dim,client,file)
    return

if __name__ == "__main__":
    main()
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    



    